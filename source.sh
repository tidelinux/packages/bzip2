# This is the source.sh script. It is executed by BPM in a temporary directory when compiling a source package
# BPM Expects the source code to be extracted into the automatically created 'source' directory which can be accessed using $BPM_SOURCE
# BPM Expects the output files to be present in the automatically created 'output' directory which can be accessed using $BPM_OUTPUT

DOWNLOAD="https://www.sourceware.org/pub/bzip2/bzip2-${BPM_PKG_VERSION}.tar.gz"
FILENAME="${DOWNLOAD##*/}"

# The prepare function is executed in the root of the temp directory
# This function is used for downloading files and putting them into the correct location
prepare() {
  wget "$DOWNLOAD"
  tar -xvf "$FILENAME" --strip-components=1 -C "$BPM_SOURCE"
}

# The build function is executed in the source directory
# This function is used to compile the source code
build() {
  sed -i 's@\(ln -s -f \)$(PREFIX)/bin/@\1@' Makefile
  sed -i "s@(PREFIX)/man@(PREFIX)/share/man@g" Makefile
  make -f Makefile-libbz2_so
  make clean
  make
}

# The package function is executed in the source directory
# This function is used to move the compiled files into the output directory
package() {
  make PREFIX="$BPM_OUTPUT"/usr install
  cp -a libbz2.so.* "$BPM_OUTPUT"/usr/lib
  ln -s libbz2.so.1.0.8 "$BPM_OUTPUT"/usr/lib/libbz2.so
  cp bzip2-shared "$BPM_OUTPUT"/usr/bin/bzip2
  for i in "$BPM_OUTPUT"/usr/bin/{bzcat,bunzip2}; do
    ln -sf bzip2 $i
  done
  rm -f "$BPM_OUTPUT"/usr/lib/libbz2.a
  install -Dm644 LICENSE "$BPM_OUTPUT"/usr/share/licenses/bzip2/LICENSE
}
